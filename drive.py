from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor, InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

class drive:
    def __init__(self, left:Port, right:Port) -> None:
        self.__motorRight = Motor(right, Direction.CLOCKWISE)
        self.__motorLeft = Motor(left, Direction.CLOCKWISE)



    def __runRightMotor(self, speed):
        self.__motorRight.run(speed)

    def __runLeftMotor(self, speed):
        self.__motorLeft.run(speed)

    def __stopRightMotor(self):
        self.__motorRight.stop()

    def __stopLeftMotor(self):
        self.__motorLeft.stop()



    def stop(self):
        self.__stopRightMotor()
        self.__stopLeftMotor()

    def start(self, speed:int):
        self.__runRightMotor(speed)
        self.__runLeftMotor(speed)

    def turnLeft(self, speed:int):
        self.__runRightMotor(-abs(speed))
        self.__runLeftMotor(speed)

    def turnRight(self, speed:int):
        self.__runRightMotor(speed)
        self.__runLeftMotor(-abs(speed))

